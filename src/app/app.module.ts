import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginModule } from './login/login.module';
import { CurrencyModule } from './currency/currency.module';
import { HttpModule } from '@angular/http';
import { CurrencyCreateComponent } from './currency/create/currency-create.component';
import { CurrencyListComponent } from './currency/list/currency-list.component';
import { LoginComponent } from './login/login.component';
import { AppHeaderComponent } from './headernavbar/headerbar.component';
import { AppLeftNavBarComponent } from './leftnavbar/leftnavbar.component';

@NgModule({
  declarations: [
    AppComponent,
    CurrencyCreateComponent,
    CurrencyListComponent,
    LoginComponent,
    AppHeaderComponent,
    AppLeftNavBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    LoginModule,
    CurrencyModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
