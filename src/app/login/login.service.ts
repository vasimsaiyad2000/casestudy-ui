import { Injectable } from '@angular/core';
import { Login } from "./login";
import { Http, Response } from "@angular/http";
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import { Observable } from "rxjs/Observable";

@Injectable()
export class LoginService {

  private apiUrl = '/api/v1/user/login';
  
  constructor(private http: Http) {
  }


  login(email: string, password: string) {
        return this.http.post(this.apiUrl, { email: email, password: password })
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let user = response.json();
                if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }
            });
    }
}
