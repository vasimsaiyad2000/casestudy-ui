import { Component, OnInit } from '@angular/core';
import { Currency } from "../currency";
import { CurrencyService } from "../currency.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-currency-create',
  templateUrl: './currency-create.component.html',
  providers: [CurrencyService]
})
export class CurrencyCreateComponent implements OnInit {
  
  model: any = {};
  currencies: any = [];
  created : any = {};
  status: any = {};
  
  constructor(private currencyService: CurrencyService,
  			  private router : Router) { }

  ngOnInit() {
    
  }

  saveCurrency() {
    this.currencyService.createCurrency(this.model).subscribe(
    	created => {
    		this.created = created;
    		
    		if (this.created.status == 'CREATED') {
    			this.router.navigate(['/currency/list']);
    		}	
        },
        error =>{
        		this.status.type ='error';
        		this.status.prefix = 'Error!';
        		this.status.message = error.json().detail;
        }
    );
  }
}