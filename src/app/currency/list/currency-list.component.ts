import { Component, OnInit } from '@angular/core';
import { Currency } from "../currency";
import { CurrencyService } from "../currency.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-currency-list',
  templateUrl: './currency-list.component.html',
  providers: [CurrencyService]
})
export class CurrencyListComponent implements OnInit {
  
  model: any = {};
  currencies: any = [];
  
  constructor(private currencyService: CurrencyService,
  			  private router : Router) { }

  ngOnInit() {
    this.getAllCurrencies();
  }

  getAllCurrencies() {
    this.currencyService.getCurrencies().subscribe(
      currencies => {
        this.currencies = currencies.data;
      },
      err => {
        console.log(err);
      }
    );
  }
}