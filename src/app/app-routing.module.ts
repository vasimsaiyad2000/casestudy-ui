import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { CurrencyListComponent } from './currency/list/currency-list.component';
import { CurrencyCreateComponent } from './currency/create/currency-create.component';

const routes: Routes = [	
     { path: 'currency/list', component: CurrencyListComponent },
     { path: 'currency/create', component: CurrencyCreateComponent },
     { path: '', component: LoginComponent },
     
     // otherwise redirect to home
     { path: '**', redirectTo: '' }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
